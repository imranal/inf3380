#include <stdlib.h>
#include <stdio.h>

int main() {
    int rows = 3;
    int cols  = 4;
    // Contiguous storage
    int (*A)[cols] = malloc(sizeof *A * rows);

    printf("Printing Row-Column matrix output :\n");
    int i,j;
    for(i = 0; i<rows; i++) {
        for(j = 0; j<cols; j++) {
            A[i][j] = i+j;
            printf("%d ",A[i][j]);
        }
        printf("\n");
    }

    printf("Printing Column-Row matrix output :\n");
    for(j = 0; j<cols; j++) {
        for(i = 0; i<rows; i++)   
            printf("%d ",A[i][j]);
        printf("\n");
    }

    free(A);
    return 0;
}
