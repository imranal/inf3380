#include <stdio.h>
#include <math.h>

int main(int nargs, char** args) {
    int N;
    if (nargs == 2)
        N = atoi(args[1]);
    else    
        N = 10;

    double sum;
    sum = 0;
    int i;
    for (i = 0; i<N; i++) {
        sum += pow(-1,i)*pow(2,-2*i);
    }

    double exact = 4/5.0; 
    double diff;
    diff = fabs(exact-sum);
    
    if (diff>1e-6)
        printf("The sum %.10f has not converged towards 4/5 for the given iteration N : %d\n",sum,N);
    else
        printf("The sum %.10f converges towards 4/5 for %d iterations.\n",sum,N);

    return 0;
}
