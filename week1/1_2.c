#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int nargs, char** args) {
    int n;
    if (nargs == 2)
        n = atoi(args[1]);
    else    
        n = 10;

    // Allocate 1D array of length n :
    float *p = (float*)malloc(n*sizeof(float));

    // Assign random values :
    int i;

    if (n <= 10) { // print the random values on screen
        for (i = 0; i<n; i++) {
            float value = (float)rand()/(float)(RAND_MAX); // generates a random float [0,1]
            p[i] = value; 
            printf("%f\n",value);
        }
    }
    else {
        for (i = 0; i<n; i++) {
            float value = (float)rand()/(float)(RAND_MAX); // generates a random float [0,1]
            p[i] = value; 
        }
    }
    
    double v1,v2,tmp1,tmp2,min,max;
    min = max = p[0];
    // Find max and min
    for (i = 0; i<n-1; i++) {
            v1 = p[i];
            v2 = p[i+1];
            tmp1 = fmin(v1,v2);
            tmp2 = fmax(v1,v2);

            if (tmp1<min) min = tmp1;
            if (tmp2>max) max = tmp2;
    }
    printf("\nThe Max value is : %f and min value : %f.\n",max,min);

    // Always free the allocated memory!
    free(p);
    return 0;
}
